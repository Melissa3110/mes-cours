#Histoire & Explication du CSS

Le CSS ou Cascading Style Sheets (feuilles de style en cascade), est un langage informatique incontournable pour la mise en forme des pages sur internet.

Ça ne vous en dit pas beaucoup plus, mais sachez qu’aujourd’hui, tous les sites Internet possèdent des informations en CSS.

Tout cela semble très basique, mais aujourd’hui le CSS permet bien des subtilités, et même d’animer des éléments.

Dans tous les cas, il organise les différentes parties d’un site.

Après tout, regardez comment sont généralement organisés les sites web :

Un header (l’entête du site)
Un menu (contenu dans l’entête, sur le côté ou quand on clique sur un bouton (surtout sur mobile)
Le contenu principal
Un footer (le bas de page)
Tout cela est parfaitement organisé grâce au CSS. Comme le sont tous les éléments à l’intérieur : blocs de texte, vidéos, images, boutons…

Et le site garde une cohérence entre chaque page grâce aussi à l’utilisation de ce CSS.

Notez que la gestion de ces feuilles de style relève de la compétence du Développeur Front-End.

#Essaie/Prise de note


**CSS PROJET**


###Style2.CSS

###Code Color 


####****-Blue****
####*****-Brown*****
####*****-Coral***** 
####****-CadeBlue****

********

###Ecriture

Liste 

*-Amiko*

***-Georgia***

-Calibri



#Background-color: 

**Code Couleur exemple **

Purple;
width: 900px;


Darkgreen: 10px 10px 10px rgb(224, 172, 14)


